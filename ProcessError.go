package KylinComponents

import (
	"fmt"
	"github.com/gin-gonic/gin"
)

func PrintErr(err error) {
	if err != nil {
		fmt.Println(err)
	}
}

func LoggerErr(c *gin.Context, err error) {
	if err != nil {
		LogError(c, fmt.Sprintf("%s", err))
	}
}

func PanicErr(err error) {
	if err != nil {
		panic(err)
	}
}
