package KylinComponents

import (
	"fmt"
	"github.com/idoubi/goz"
)

func CurlGet(url string) (*goz.Response, error) {
	cli := goz.NewClient()
	go LogDebug(nil, "Get request:"+url)
	resp, err := cli.Get(url)
	if err != nil {
		//log.Fatalln(err)
		go LogError(nil, fmt.Sprintf("%s", err))
		return nil, err
	} else {
		return resp, nil
	}
}

func CurlGetParam(url string) (*goz.Response, error) {
	cli := goz.NewClient()

	resp, err := cli.Get(url)
	if err != nil {
		//log.Fatalln(err)
		return nil, err
	} else {
		return resp, nil
	}
}
func CurlPostWithParam(url string, headers map[string]interface{}, params map[string]interface{}) (*goz.Response, error) {
	cli := goz.NewClient()
	go LogDebug(nil, "Post request:"+url)

	resp, err := cli.Post(url,
		goz.Options{
			Headers:    headers,
			FormParams: params,
		})
	if err != nil {
		//log.Fatalln(err)
		return nil, err
	} else {
		return resp, nil
	}
}

func CurlPutWithParam(url string, headers map[string]interface{}, params map[string]interface{}) (*goz.Response, error) {
	cli := goz.NewClient()
	go LogDebug(nil, "Put request:"+url)

	resp, err := cli.Put(url,
		goz.Options{
			Headers:    headers,
			FormParams: params,
		})
	if err != nil {
		//log.Fatalln(err)
		return nil, err
	} else {
		return resp, nil
	}
}

func CurlDeleteWithParam(url string, headers map[string]interface{}, params map[string]interface{}) (*goz.Response, error) {
	cli := goz.NewClient()
	go LogDebug(nil, "Delete request:"+url)
	//遗留问题 delete 请求params参数无法识别，
	//goz request库中对delete方法处理body参数为空：case http.MethodGet, http.MethodDelete: http.NewRequest(method, uri, body: nil)
	resp, err := cli.Delete(url,
		goz.Options{
			Headers:    headers,
			FormParams: params,
		})
	if err != nil {
		//log.Fatalln(err)
		return nil, err
	} else {
		return resp, nil
	}
}

func CurlPostWithJson(url string, headers map[string]interface{}, params interface{}) (*goz.Response, error) {
	cli := goz.NewClient()
	go LogDebug(nil, "Post request:"+url)

	resp, err := cli.Post(url,
		goz.Options{
			Headers: headers,
			JSON:    params,
		})
	if err != nil {
		//log.Fatalln(err)
		return nil, err
	} else {
		return resp, nil
	}
}
