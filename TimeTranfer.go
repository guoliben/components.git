package KylinComponents

import (
	"database/sql"
	"fmt"
	"strings"
	"time"
)

var weekDay = map[string]int64{
	"Sunday":    0,
	"Monday":    1,
	"Tuesday":   2,
	"Wednesday": 3,
	"Thursday":  4,
	"Friday":    5,
	"Saturday":  6,
}
var DayTimeFormat = "2006-01-02"
var MonthTimeFormat = "2006-01"

func UtcToLocal(utcTime string) string {
	if utcTime == "" {
		return ""
	}
	t, err := time.Parse("2006-01-02T15:04:05+0800", utcTime)
	if err != nil {
		fmt.Println(err)
	}
	return t.Local().Format("2006-01-02 15:04:05")
}
func UtcToLocal2(utcTime string) string {
	if utcTime == "" {
		return ""
	}
	t, err := time.Parse("2006-01-02T15:04:05+08:00", utcTime)
	if err != nil {
		fmt.Println(err)
	}
	return t.Local().Format("2006-01-02 15:04:05")
}
func UTCTransLocalEight(utcTime string) string {
	t, _ := time.Parse("2006-01-02T15:04:05.000+08:00", utcTime)
	return t.Local().Format("2006-01-02 15:04:05")
}
func UTCTransLocal(utcTime string) string {
	t, _ := time.Parse("2006-01-02T15:04:05.000+00:00", utcTime)
	return t.Local().Format("2006-01-02 15:04:05")
}

func TZToLocal(utcTime string) string {
	if utcTime == "" {
		return ""
	}
	t, err := time.Parse("2006-01-02T15:04:05.000Z", utcTime)
	if err != nil {
		fmt.Println(err)
	}
	return t.Local().Format("2006-01-02 15:04:05")
}
func ToLocal(utcTime string) string {
	if utcTime == "" {
		return ""
	}
	if strings.Contains(utcTime, "Z") {
		return TZToLocal(utcTime)
	} else if strings.Contains(utcTime, "000+08:00") {
		return UTCTransLocalEight(utcTime)
	} else if strings.Contains(utcTime, "000+00:00") {
		return UTCTransLocal(utcTime)
	} else if strings.Contains(utcTime, "+08:00") {
		return UtcToLocal2(utcTime)
	} else if strings.Contains(utcTime, "+0800") {
		return UtcToLocal(utcTime)
	} else {
		return ""
	}
}

func TimeToLocal(utcTime time.Time) string {
	return utcTime.Format("2006-01-02 15:04:05")
}

/**
 * 将日期处理为格式
 * dimension ：day-天 week-周 month-月
 */
func ParseDays(startDate string, endDate string, dimension string) []string {
	var dateList []string
	startDateToFormat, _ := time.Parse(DayTimeFormat, startDate)
	endDateToFormat, _ := time.Parse(DayTimeFormat, endDate)

	if endDateToFormat.Unix() < startDateToFormat.Unix() { // 起始日期大于截止日期
		return nil
	}
	if dimension == "day" { //以天为单位返回数据
		for i := startDateToFormat.Unix(); i <= endDateToFormat.Unix(); i += 86400 {
			tm := time.Unix(i, 0)
			dateList = append(dateList, tm.Format(DayTimeFormat))
		}
	} else if dimension == "week" { //以周为单位返回数据
		weekKey := startDateToFormat.Weekday()
		weekKeyEnd := endDateToFormat.Weekday()
		newStartDate := startDateToFormat.Unix() - (86400 * weekDay[weekKey.String()])
		newEndDate := endDateToFormat.Unix() + 86400*(6-weekDay[weekKeyEnd.String()])
		for i := newStartDate; i <= newEndDate; i += 7 * 86400 {
			tm := time.Unix(i, 0)
			dateList = append(dateList, tm.Format(DayTimeFormat))
		}
	} else if dimension == "month" { //以月为单位返回数据
		i := GetFirstDateOfMonth(startDateToFormat)
		for {
			dateList = append(dateList, i.Format(MonthTimeFormat))
			i = i.AddDate(0, 1, 0)
			if i.Unix() > endDateToFormat.Unix() {
				break
			}
		}
	} else {
		return nil
	}
	return dateList
}
func GetFirstDateOfMonth(d time.Time) time.Time {
	d = d.AddDate(0, 0, -d.Day()+1)
	return d
}
func GetFirstDateOfWeek(d time.Time) time.Time {
	//date, _ := time.Parse(DayTimeFormat, d)

	newD := d.Unix() - (86400 * weekDay[d.Weekday().String()])
	return time.Unix(newD, 0)
}

func FormatSqlTime(t sql.NullTime) string {
	if t.Valid {
		return t.Time.Format("2006-01-02 15:04:05")
	} else {
		return ""
	}
}
