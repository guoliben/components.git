package KylinComponents

import "strings"

var ForbiddenStr = [256]string{
	"su",
	"sudo",
	"bash",
	"sh",
	"..",
}

func Cmd(command string) string {
	for _, v := range ForbiddenStr {
		strings.ReplaceAll(command, v, "")
	}
	return command
}
