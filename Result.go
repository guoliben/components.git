package KylinComponents

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/idoubi/goz"
	"net/http"
)

func ResultSuccess(data map[string]interface{}) map[string]interface{} {
	return gin.H{"code": 0, "msg": "成功", "result": JsonCamelCase{Value: data}}
}

func ResultError(key int, data map[string]string) map[string]interface{} {
	msg := ErrorMsg(key)
	return gin.H{"code": msg.code, "msg": msg.msg, "result": data}
}

/*
 * 处理API返回值 -- GET请求
 */
func ProcessGetCode(c *gin.Context, resp *goz.Response, result interface{}) map[string]interface{} {
	if resp.GetStatusCode() == http.StatusUnauthorized { //gitlab-token验证失败
		go LogDebug(c, RunFuncName(), "token验证失败", IntToString(resp.GetStatusCode()))
		return ResultError(GITLAB_SERVICE_UNAUTHORIZED, nil)
	} else if resp.GetStatusCode() != http.StatusOK {
		data, err := resp.GetBody()
		go LoggerErr(c, err)
		go LogDebug(c, RunFuncName(), "返回错误状态码", IntToString(resp.GetStatusCode()), data.String())
		return ResultError(GITLAB_REQUEST_FAILED, map[string]string{
			"error": data.String(),
		})
	} else {
		data, err := resp.GetBody()
		go LoggerErr(c, err)

		err = json.Unmarshal(data, &result)
		go LoggerErr(c, err)
		go LogDebug(c, RunFuncName(), string(JsonMar(result)))

		ret := make(map[string]interface{})
		ret["list"] = result
		headerResp := resp.GetHeaders()
		if _, ok := headerResp["X-Total"]; ok {
			if len(headerResp["X-Total"]) > 0 {
				total := headerResp["X-Total"][0] //check undefined key : avoid index out of range [0]
				ret["total"] = StringToInt(total)
				fmt.Println(total, "---->")
			}
		} else {
			ret["total"] = 0
		}
		return ResultSuccess(ret)
	}
}

/*
 * 处理API返回值 -- GET请求
 */
//func processResultResultSuccess(c *gin.Context, resp *goz.Response,result interface{}) map[string]interface{} {
//	data, err := resp.GetBody()
//	go LoggerErr(c, err)
//
//	err = json.Unmarshal(data, &result)
//	go LoggerErr(c, err)
//	go LogDebug(c, RunFuncName(), string(JsonMar(result)))
//
//	ret := make(map[string]interface{})
//	ret["list"] = result
//	return Result.ResultSuccess(ret)
//}

/*
 * 处理API返回值 -- POST请求
 */
func ProcessPostCode(c *gin.Context, resp *goz.Response, result interface{}) map[string]interface{} {
	if resp.GetStatusCode() == http.StatusUnauthorized { //gitlab-token验证失败
		go LogDebug(c, RunFuncName(), "token验证失败", IntToString(resp.GetStatusCode()))
		return ResultError(GITLAB_SERVICE_UNAUTHORIZED, map[string]string{
			"error": "token验证失败",
		})
	} else if resp.GetStatusCode() != http.StatusCreated { //201 创建类型接口状态码
		data, err := resp.GetBody()
		go LoggerErr(c, err)
		go LogDebug(c, RunFuncName(), "返回错误状态码", IntToString(resp.GetStatusCode()), data.String())
		return ResultError(GITLAB_CREATE_FAILED, map[string]string{
			"error": data.String(),
		})
	} else {
		data, err := resp.GetBody()
		go LoggerErr(c, err)

		err = json.Unmarshal(data, &result)
		go LoggerErr(c, err)
		go LogDebug(c, RunFuncName(), string(JsonMar(result)))

		ret := make(map[string]interface{})
		ret["list"] = result

		return ResultSuccess(ret)
	}
}

/*
 * 处理API返回值 -- PUT请求
 */
func ProcessPutCode(c *gin.Context, resp *goz.Response, result interface{}) map[string]interface{} {
	if resp.GetStatusCode() == http.StatusUnauthorized { //gitlab-token验证失败
		go LogDebug(c, RunFuncName(), "token验证失败", IntToString(resp.GetStatusCode()))
		return ResultError(GITLAB_SERVICE_UNAUTHORIZED, map[string]string{
			"error": "token验证失败",
		})
	} else if resp.GetStatusCode() != http.StatusOK { //200 修改类型接口状态码
		data, err := resp.GetBody()
		go LoggerErr(c, err)
		go LogDebug(c, RunFuncName(), "返回错误状态码", IntToString(resp.GetStatusCode()))
		return ResultError(GITLAB_UPDATE_FAILED, map[string]string{
			"error": data.String(),
		})
	} else {
		data, err := resp.GetBody()
		go LoggerErr(c, err)

		err = json.Unmarshal(data, &result)
		go LoggerErr(c, err)
		go LogDebug(c, RunFuncName(), string(JsonMar(result)))

		ret := make(map[string]interface{})
		ret["list"] = result
		return ResultSuccess(ret)
	}
}

/*
 * 处理API返回值 -- DELETE请求
 */
func ProcessDeleteCode(c *gin.Context, resp *goz.Response, result interface{}) map[string]interface{} {

	if resp.GetStatusCode() == http.StatusUnauthorized { //gitlab-token验证失败
		go LogDebug(c, RunFuncName(), "token验证失败", IntToString(resp.GetStatusCode()))
		return ResultError(GITLAB_SERVICE_UNAUTHORIZED, map[string]string{
			"error": "token验证失败",
		})
	} else if resp.GetStatusCode() != http.StatusNoContent { //204 删除类型接口状态码
		go LogDebug(c, RunFuncName(), "返回错误状态码", IntToString(resp.GetStatusCode()))
		data, err := resp.GetBody()
		go LoggerErr(c, err)
		return ResultError(GITLAB_DELETE_FAILED, map[string]string{
			"error": data.String(),
		})
	} else {
		data, err := resp.GetBody()
		go LoggerErr(c, err)

		err = json.Unmarshal(data, &result)
		go LoggerErr(c, err)
		go LogDebug(c, RunFuncName(), string(JsonMar(result)))

		ret := make(map[string]interface{})
		ret["list"] = result
		return ResultSuccess(ret)
	}
}
