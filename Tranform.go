package KylinComponents

import (
	"errors"
	"strconv"
)

func StringToInt(str string) int {
	i, err := strconv.Atoi(str)
	if err != nil {
		//
	}
	return i
}

func StringToIntWithError(str string) (int, error) {
	i, err := strconv.Atoi(str)
	if err != nil {
		return 0, err
	}
	return i, nil
}

func StringToInt64(str string) int64 {
	int64, err := strconv.ParseInt(str, 10, 64)
	if err != nil {
		//
	}
	return int64
}

func IntToString(i int) string {
	return strconv.Itoa(i)
}

func Int64ToString(i int64) string {
	return strconv.FormatInt(i, 10)
}
func StringSliceCut(arr []string, from int, to int) ([]string, error) {
	if from > to {
		return nil, errors.New("'From' is Not allow bigger than 'To'")
	}
	if from < 0 || to < 0 {
		return nil, errors.New("'From' is Not allow bigger than 'To'")
	}
	if from > len(arr) {
		return nil, errors.New("'From' is Not allow bigger than 'len(arr)'")
	}
	if to > len(arr) {
		return arr[from:], nil
	} else if from <= to {
		return arr[from:to], nil
	} else {
		return arr, nil
	}
}
