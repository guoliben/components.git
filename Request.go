package KylinComponents

import (
	"bytes"
	"crypto/rand"
	"github.com/gin-gonic/gin"
	"math/big"
)

func reqId() string {
	len := 8
	var container string
	var str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
	b := bytes.NewBufferString(str)
	length := b.Len()
	bigInt := big.NewInt(int64(length))
	for i := 0; i < len; i++ {
		randomInt, _ := rand.Int(rand.Reader, bigInt)
		container += string(str[randomInt.Int64()])
	}
	return container
}

func SetRequestId() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Set("reqId", reqId())
		c.Next()

	}
}
