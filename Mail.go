package KylinComponents

import (
	"fmt"
	"strings"
)

const (
	//FROM_EMAIL = "guoliben8@qq.com"
	//FROM_HOST = "smtp.qq.com"
	//FROM_PORT = 587
	//FROM_ACCOUNT = "guoliben8"
	//FROM_SECRET = "ehupttnjnbhebhjc"
	FROM_EMAIL = "guoliben@kylinos.cn"
	//FROM_HOST = "172.17.111.24"
	//FROM_HOST = "mailgw.kylinos.cn"
	FROM_HOST    = "172.17.111.24:25"
	FROM_PORT    = 25
	FROM_ACCOUNT = "guoliben"
	FROM_SECRET  = "1(T&pi~(4S2"
)

func SendByKylin(mail string, subject string, body string) {
	fmt.Println("send email")
	err := SendToMail(FROM_EMAIL, FROM_SECRET, FROM_HOST, mail, subject, body, "html")
	if err != nil {
		fmt.Println("Send mail error!")
		fmt.Println(err)
	} else {
		fmt.Println("Send mail success!")
	}
}

func SendToMail(user, password, host, to, subject, body, mailtype string) error {
	hp := strings.Split(host, ":")
	//auth := smtp.PlainAuth("", user, password, hp[0])
	auth := PlainAuth("", user, password, hp[0])
	var content_type string
	if mailtype == "html" {
		content_type = "Content-Type: text/" + mailtype + "; charset=UTF-8"
	} else {
		content_type = "Content-Type: text/plain" + "; charset=UTF-8"
	}

	msg := []byte("To: " + to + "\r\nFrom: " + user + ">\r\nSubject: " + "\r\n" + content_type + "\r\n\r\n" + body)
	send_to := strings.Split(to, ";")
	err := SendMail(host, auth, user, send_to, msg)
	return err
}
