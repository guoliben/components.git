package KylinComponents

const (
	USER_NOT_EXIST            = 1000
	USER_SESSION_CHECK_FAILED = 1001
	USER_OR_PASS_ERROR        = 1002
	TOKEN_MISSING             = 1003
	TOKEN_VALID               = 1004
	USER_PASSWORD_ERROR       = 1005
	ADMIN_AUTH_CHECK_FAILED   = 1006
	CREATE_USER_FAILED        = 1007

	PARAMETER_ERROR   = 2000
	PARAMETER_MISSING = 2001

	DB_UPDATE_FAILED = 3000
	DB_NOT_EXIST     = 3001

	FILE_UPLOAD_FAILED   = 4000
	FILE_DOWNLOAD_FAILED = 4001

	GITLAB_SERVICE_FAILED       = 5000
	GITLAB_SERVICE_UNAUTHORIZED = 5001
	GITLAB_AUTHORIZED_MISSING   = 5002
	GITLAB_MERGE_EXIST          = 5003
	GITLAB_MERGE_NOTEXIST       = 5004
	GITLAB_CREATE_FAILED        = 5010
	GITLAB_DELETE_FAILED        = 5011
	GITLAB_UPDATE_FAILED        = 5012
	GITLAB_REQUEST_FAILED       = 5013

	SONAR_SERVICE_FAILED       = 5100
	SONAR_SERVICE_UNAUTHORIZED = 5101
	SONAR_AUTHORIZED_MISSING   = 5102

	JENKINS_SERVICE_FAILED       = 5200
	JENKINS_SERVICE_UNAUTHORIZED = 5201
	JENKINS_AUTHORIZED_MISSING   = 5202
	JENKINS_BUILD_PARAM_ERROR    = 5203
	JENKINS_BUILD_UNAUTHORIZED   = 5204
)

type CodeMsg struct {
	code int
	msg  string
}

func ErrorMsg(key int) CodeMsg {
	msg := ""
	switch key {
	case USER_NOT_EXIST:
		msg = "用户不存在"
	case USER_SESSION_CHECK_FAILED:
		msg = "用户登录超时"
	case USER_OR_PASS_ERROR:
		msg = "用户名或密码错误"
	case TOKEN_MISSING:
		msg = "token缺失"
	case TOKEN_VALID:
		msg = "token无效"
	case USER_PASSWORD_ERROR:
		msg = "密码错误"
	case ADMIN_AUTH_CHECK_FAILED:
		msg = "您没有管理员权限"
	case CREATE_USER_FAILED:
		msg = "创建用户失败"

	case PARAMETER_ERROR:
		msg = "参数错误"
	case PARAMETER_MISSING:
		msg = "参数缺失"
	case DB_UPDATE_FAILED:
		msg = "数据库操作失败"
	case DB_NOT_EXIST:
		msg = "数据不存在"

	case FILE_UPLOAD_FAILED:
		msg = "文件上传失败"
	case FILE_DOWNLOAD_FAILED:
		msg = "文件下载失败"

	case GITLAB_SERVICE_FAILED:
		msg = "代码托管服务不可用，请稍后再试"
	case GITLAB_SERVICE_UNAUTHORIZED:
		msg = "代码托管服务授权失败，请联系管理员"
	case GITLAB_AUTHORIZED_MISSING:
		msg = "代码托管服务授权码缺失请联系管理员"
	case GITLAB_MERGE_EXIST:
		msg = "合并请求已经存在，请勿重复提交"
	case GITLAB_MERGE_NOTEXIST:
		msg = "请求记录不存在"
	case GITLAB_REQUEST_FAILED:
		msg = "请求失败"
	case GITLAB_CREATE_FAILED:
		msg = "创建失败"
	case GITLAB_DELETE_FAILED:
		msg = "删除失败"
	case GITLAB_UPDATE_FAILED:
		msg = "修改失败"

	case SONAR_SERVICE_FAILED:
		msg = "代码检查服务不可用，请稍后再试"
	case SONAR_SERVICE_UNAUTHORIZED:
		msg = "代码检查服务授权失败，请联系管理员"
	case SONAR_AUTHORIZED_MISSING:
		msg = "代码检查服务授权码缺失请联系管理员"

	case JENKINS_SERVICE_FAILED:
		msg = "流水线服务不可用，请稍后再试"
	case JENKINS_SERVICE_UNAUTHORIZED:
		msg = "流水线服务授权失败，请在个人中心更新token"
	case JENKINS_AUTHORIZED_MISSING:
		msg = "流水线服务授权码缺失，请在个人中心更新token"
	case JENKINS_BUILD_PARAM_ERROR:
		msg = "流水线运行失败，请检查参数是否正确"
	case JENKINS_BUILD_UNAUTHORIZED:
		msg = "您无权运行此流水线，如需开通，请联系管理员"
	}
	return CodeMsg{
		code: key,
		msg:  msg,
	}
}
