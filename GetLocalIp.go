package KylinComponents

import (
	"fmt"
	"net"
)

func GetLocalIp() string {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		LogError(nil, fmt.Sprintf("%s", err))
		return ""
	}
	for _, address := range addrs {
		// 检查ip地址判断是否回环地址
		if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				return ipnet.IP.String()
			}
		}
	}
	return ""
}

func GetLocalMac() string {
	netInterfaces, err := net.Interfaces()
	if err != nil {
		LogError(nil, fmt.Sprintf("%s", err))
		return ""
	}
	for _, netInterface := range netInterfaces {
		macAddr := netInterface.HardwareAddr.String()
		if len(macAddr) == 0 {
			continue
		}
		return macAddr
	}
	return ""
}
