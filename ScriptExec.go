package KylinComponents

import (
	"fmt"
	"os/exec"
)

func Run(command string) []byte {
	cmd := exec.Command("/bin/bash", "-c", command)
	bytes, err := cmd.Output()
	if err != nil {
		LogError(nil, fmt.Sprintf("%s", err))
		return nil
	}
	return bytes

}
