package KylinComponents

import (
	"fmt"
	"github.com/Unknwon/goconfig"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

var cfg *goconfig.ConfigFile

/**
 * 动态加载配置文件任务
 */
func LoadEnvByCronTask() {
	//spec := "* */1 * * * ?"
	//TaskCron.CronLoad(spec, LoadEnv)
}

/**
 * 加载配置文件
 */
func LoadEnv() {
	env, err := ReadEnv(".env")
	if err != nil {
		fmt.Println(err)
	}
	cfg, err = goconfig.LoadConfigFile(strings.Replace(string(env), "\n", "", -1))
	if err != nil {
		log.Fatalf("无法加载配置文件：%s", err)
	}
}

/**
 * 获取配置文件 单例 + 动态加载
 */
func InitConfigFromToml() *goconfig.ConfigFile {
	if cfg == nil {
		LoadEnv()
	}
	return cfg
}

/**
 * 读取配置文件
 */
func ReadEnv(filePth string) ([]byte, error) {
	f, err := os.Open(filePth)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	return ioutil.ReadAll(f)
}
