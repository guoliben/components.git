package KylinComponents
import (
	"math/rand"
	"time"
)

var randBytes = []byte("1234567890_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
func init() {
	rand.Seed(time.Now().UnixNano())
}
func GetRandomString(s int) string {
	ret := make([]byte, s)
	for i := 0; i < s; i++ {
		ret[i] = randBytes[rand.Int31()%62]
	}
	return string(ret)
}