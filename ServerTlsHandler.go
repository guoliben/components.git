package KylinComponents

import (
	"github.com/gin-gonic/gin"
	"github.com/unrolled/secure"
)

func TlsHandler(host string, port string) gin.HandlerFunc {
	return func(c *gin.Context) {
		secureMiddleWare := secure.New(secure.Options{
			SSLRedirect: true,
			SSLHost:     host + ":" + port,
		})
		err := secureMiddleWare.Process(c.Writer, c.Request)
		if err != nil {
			return
		}
		c.Next()
	}
}
